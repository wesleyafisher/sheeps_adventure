#include <iostream>
#include <cmath>

#include "projectile.h"

void Projectile::update(Uint32 ticks) {
  TwoWayMultiSprite::advanceFrame(ticks, getVelocity()[0]);

  /*float yincr = getVelocityY() * static_cast<float>(ticks) * 0.001;
  setY( getY()- yincr );
  float xincr = getVelocityX() * static_cast<float>(ticks) * 0.001;
  setX( getX()- xincr );*/

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr);

  //distance += ( hypot(xincr, yincr) );
  distance += ( hypot(incr[0], incr[1]) );

  if (distance > maxDistance) tooFar = true;
}

