#include "twoWayMultiSprite.h"
#include "gameData.h"
#include "renderContext.h"
#include "explodingSprite.h"

TwoWayMultiSprite::~TwoWayMultiSprite(){ if(explosion) delete explosion; }

void TwoWayMultiSprite::advanceFrame(Uint32 ticks, const float xVelocity){
  timeSinceLastFrame += ticks;

	if (timeSinceLastFrame > frameInterval) {
    // Last moving velocity of the sprite
    static float lastXvelocity = 0.0;

    // The sprite isn't moving
    if(xVelocity == 0.0){
      // It was going right the last time it was moving
      if(lastXvelocity >= 0.0) currentFrame = rightWrap(0);

      // It going left the last time it was moving
      else currentFrame = leftWrap(0);
    }

    // The sprite is moving to the right
    else if(xVelocity > 0.0){
      currentFrame = rightWrap(currentFrame + 1);
      lastXvelocity = xVelocity;
    }

    // The sprite is moving to the left
    else{
      currentFrame = leftWrap(currentFrame + 1);
      lastXvelocity = xVelocity;
    }

    timeSinceLastFrame = 0;
	}

}

TwoWayMultiSprite::TwoWayMultiSprite( const std::string& name) :
  Drawable(name, 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/startLoc/x"), 
                    Gamedata::getInstance().getXmlInt(name+"/startLoc/y")), 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/speedX"),
                    Gamedata::getInstance().getXmlInt(name+"/speedY")),
           Gamedata::getInstance().getXmlInt(name+"/type")
           ),
  images( RenderContext::getInstance().getImages(name) ),
  explosion(nullptr),
  currentFrame(0),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  frameInterval( Gamedata::getInstance().getXmlInt(name+"/frameInterval")),
  timeSinceLastFrame( 0 ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height"))
{ }

TwoWayMultiSprite::TwoWayMultiSprite(const std::string& name, const Vector2f& pos, const Vector2f& vel) :
  Drawable(name, pos, vel, Gamedata::getInstance().getXmlInt(name+"/type")),
  images( RenderContext::getInstance().getImages(name) ),
  explosion(nullptr),
  currentFrame(0),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  frameInterval( Gamedata::getInstance().getXmlInt(name+"/frameInterval")),
  timeSinceLastFrame( 0 ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height"))
{ }

TwoWayMultiSprite::TwoWayMultiSprite(const TwoWayMultiSprite& s) :
  Drawable(s), 
  images(s.images),
  explosion(s.explosion),
  currentFrame(s.currentFrame),
  numberOfFrames( s.numberOfFrames ),
  frameInterval( s.frameInterval ),
  timeSinceLastFrame( s.timeSinceLastFrame ),
  worldWidth( s.worldWidth ),
  worldHeight( s.worldHeight )
  { }

TwoWayMultiSprite& TwoWayMultiSprite::operator=(const TwoWayMultiSprite& s) {
  Drawable::operator=(s);
  images = (s.images);
  explosion = (s.explosion);
  currentFrame = (s.currentFrame);
  numberOfFrames = ( s.numberOfFrames );
  frameInterval = ( s.frameInterval );
  timeSinceLastFrame = ( s.timeSinceLastFrame );
  worldWidth = ( s.worldWidth );
  worldHeight = ( s.worldHeight );
  return *this;
}

void TwoWayMultiSprite::explode() {
  if ( !explosion ) {
    Sprite 
    sprite(getName(), getPosition(), getVelocity(), images[currentFrame]);
    explosion = new ExplodingSprite(sprite);
  }
  deactivate();
}

void TwoWayMultiSprite::draw() const {
  if(explosion) explosion->draw();
  if(isActive()) images[currentFrame]->draw(getX(), getY(), getScale());
}

void TwoWayMultiSprite::uiDraw() const {
  if(explosion) explosion->uiDraw();
  if(isActive()) images[currentFrame]->uiDraw(getX(), getY(), getScale());
}

void TwoWayMultiSprite::update(Uint32 ticks) {
  if ( explosion ) {
    explosion->update(ticks);
    /*if ( explosion->chunkCount() == 0 ) {
      delete explosion;
      explosion = NULL;
      activate();
    }*/
    return;
  }
  if(!isActive()) return;

  advanceFrame(ticks, getVelocity()[0]);

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr);

  if ( getY() < 0) {
    setVelocityY( fabs( getVelocityY() ) );
  }
  if ( getY() > worldHeight-getScaledHeight()) {
    setVelocityY( -fabs( getVelocityY() ) );
  }

  if ( getX() < 0) {
    setVelocityX( fabs( getVelocityX() ) );
  }
  if ( getX() > worldWidth-getScaledWidth()) {
    setVelocityX( -fabs( getVelocityX() ) );
  }  

}
