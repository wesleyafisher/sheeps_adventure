#ifndef PLAYER__H
#define PLAYER__H
#include <string>
#include <vector>
#include <list>
#include <cmath>
#include <SDL.h>
#include "projectile.h"
#include "smartSprite.h"
#include "collisionStrategy.h"
#include "sound.h"

class ExplodingSprite;

class Player : public Drawable {
public:
  ~Player();
  Player(const std::string&);
  Player(const Player&);

  virtual void draw() const;
  virtual void uiDraw() const;
  virtual void update(Uint32 ticks);
  virtual void explode();

  virtual const Image* getImage() const { 
    return images[currentFrame]; 
  }
  int getScaledWidth()  const { 
    return getScale()*images[currentFrame]->getWidth(); 
  } 
  int getScaledHeight()  const { 
    return getScale()*images[currentFrame]->getHeight(); 
  }
  int getHealth() const{
    return currentHealth;
  }
  int getAmmo() const{
    return currentAmmo;
  }
  /*int getAmmoPoolSize() const{
    return ammoPool.size();
  }
  int getFreePoolSize() const{
    return freePool.size();
  }*/
  int getGrassLeft() const{
    return grassLeft;
  }
  bool isGod() const{
    return god;
  }

  virtual const SDL_Surface* getSurface() const { 
    return images[currentFrame]->getSurface();
  }

  void becomeGod() {god = true;}
  void becomeMortal() {god = false;}

  void attach(SmartSprite* o) {observers.emplace_back(o);} 
  void detach(SmartSprite* o);

  bool checkForProjectileCollisions (Drawable&) const;

  void right();
  void left();
  void jump();
  void stop();

  void takeDamage(unsigned currentTick, SDLSound&);
  void eat(Drawable*, SDLSound&);
  void bah(unsigned currentTick, SDLSound&);

protected:
  std::vector<Image*> images;
  std::vector<SmartSprite*> observers;
  std::list<Projectile*> ammoPool;
  std::list<Projectile*> freePool;
  std::vector<Drawable*> filledHP;
  std::vector<Drawable*> emptyHP;
  std::vector<Drawable*> filledAmmo;
  std::vector<Drawable*> emptyAmmo;

  CollisionStrategy* strategy;
  ExplodingSprite* explosion;
  Drawable* grassSprite;
  SDL_Rect grassLeftRect;

  unsigned currentFrame;
  unsigned numberOfFrames;
  unsigned frameInterval;
  float timeSinceLastFrame;
  int worldWidth;
  int worldHeight;

  Vector2f initialVelocity;
  float lastXvelocity;
  int gravity;

  int maxHealth;
  int currentHealth;
  unsigned iframeDuration;
  unsigned lastTickHit;
  unsigned bahBlastCooldown;
  unsigned lastTickBahBlast;
  int maxAmmo;
  int currentAmmo;
  int grassLeft;
  bool god;

  unsigned rightWrap(unsigned num) { return num % (numberOfFrames / 2); }
  unsigned leftWrap(unsigned num) { return (num % (numberOfFrames / 2)) + (numberOfFrames / 2); }
  
  void advanceFrame(Uint32 ticks, float xVelocity);
  Player& operator=(const Player&);
};
#endif
