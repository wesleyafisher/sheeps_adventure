#include <vector>
#include <SDL.h>
#include "ioMod.h"
#include "renderContext.h"
#include "clock.h"
#include "world.h"
#include "viewPort.h"
#include "ui.h"
#include "player.h"
#include "sound.h"

class Engine {
public:
  Engine ();
  ~Engine ();
  bool play();
  void switchSprite();

  Engine(const Engine&) = delete;
  Engine& operator=(const Engine&) = delete;

private:
  //const RenderContext& rc;
  //const IoMod& io;
  //Clock& clock;

  SDL_Renderer* const renderer;
  //World world;
  World sky;
  World fargrass;
  World neargrass;
  UI& ui;
  Viewport& viewport;
  SDLSound sound;

  //Drawable* star;
  //Drawable* spinningStar;
  Player* player;
	std::vector<Drawable*> sprites;

  int winTime;
  int currentSprite;
  CollisionStrategy* strategy;
  //bool collision;
  Drawable* objectiveCollision;
  Drawable* resourceCollision;

  bool makeVideo;

  void draw() const;
  void update(Uint32);

  void printScales() const;
  void checkForCollisions();
};
