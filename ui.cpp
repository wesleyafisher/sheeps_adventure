#include <sstream>
#include "ui.h"
#include "ioMod.h"
#include "renderContext.h"
#include "gameData.h"
#include "sprite.h"

UI& UI::getInstance() {
  static UI instance;
  return instance;
}

UI::~UI() {

}

UI::UI() :
  on(false),
  renderer(RenderContext::getInstance().getRenderer()),
  uiWindow({5, 5, Gamedata::getInstance().getXmlInt("ui/width"), Gamedata::getInstance().getXmlInt("ui/height")})
{
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 140);
}

void UI::toggle(){
  if(on) on = false;
  else on = true;
}

//void UI::draw(int frametime) const {
void UI::draw(unsigned timeSinceStart, unsigned frametime, bool isGod) const {

  if(!on){
    SDL_Rect helpWindow = {5, 5, 190, 40};
    SDL_RenderFillRect(renderer, &helpWindow);
    IoMod::getInstance().writeText("Press F1 for help", {255, 255, 255, 255}, 10, 10);
    return;
  }

  // Make window
  SDL_RenderFillRect(renderer, &uiWindow);

  // Write time since start
  std::stringstream secondsSinceStart;
  secondsSinceStart << "Time: " << timeSinceStart << "s";
  IoMod::getInstance().writeText(secondsSinceStart.str(), {255, 255, 255, 255}, 10, 10);

  // Write "Frametime:#"
  std::stringstream frametimestrm;
  frametimestrm << "Frametime: " << frametime << "ms";
  //std::string currentFrametime = frametimestrm.str();
  IoMod::getInstance().writeText(frametimestrm.str(), {255, 255, 255, 255}, 10, 40);

  // Write controls
  IoMod::getInstance().writeText("Controls", {255, 255, 255, 255}, 10, 100);
  IoMod::getInstance().writeText("Left/Right: [A]/[D]", {255, 255, 255, 255}, 10, 130);
  IoMod::getInstance().writeText("Jump: [SPACE]", {255, 255, 255, 255}, 10, 160);
  IoMod::getInstance().writeText("Eat: [MOUSE1]", {255, 255, 255, 255}, 10, 190);
  IoMod::getInstance().writeText("Bah: [MOUSE2]", {255, 255, 255, 255}, 10, 220);
  if(isGod) IoMod::getInstance().writeText("God: [G] - On", {255, 255, 255, 255}, 10, 250);
  else IoMod::getInstance().writeText("God: [G] - Off", {255, 255, 255, 255}, 10, 250);
  IoMod::getInstance().writeText("Pause: [P]", {255, 255, 255, 255}, 10, 280);
  IoMod::getInstance().writeText("Reset: [R]", {255, 255, 255, 255}, 10, 310);
  IoMod::getInstance().writeText("Menu: [ESC]/[M]", {255, 255, 255, 255}, 10, 340);
  IoMod::getInstance().writeText("Toggle HUD: [F1]", {255, 255, 255, 255}, 10, 370);



  // Write pool amounts
  /*std::stringstream ammoPoolStream;
  ammoPoolStream << "Ammo Pool: " << ammoPoolSize;
  IoMod::getInstance().writeText(ammoPoolStream.str(), {255, 255, 255, 255}, 10, 340);

  std::stringstream freePoolStream;
  freePoolStream << "Free Pool: " << freePoolSize;
  IoMod::getInstance().writeText(freePoolStream.str(), {255, 255, 255, 255}, 10, 370);*/

}