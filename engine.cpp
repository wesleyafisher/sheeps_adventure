#include <iostream>
#include <algorithm>
#include <sstream>
#include <string>
#include <random>
#include <iomanip>
#include "sprite.h"
#include "twoWayMultiSprite.h"
#include "proximitySprite.h"
#include "multiSprite.h"
#include "player.h"
#include "gameData.h"
#include "engine.h"
#include "frameGenerator.h"

Engine::~Engine() {
  //delete star;
  //delete spinningStar;
  delete player;
	for(Drawable *s : sprites){
    //std::cout << "deleting sprites" << std::endl;
		delete s;
	}
  delete strategy;
  //delete &(UI::getInstance());
  std::cout << "Terminating program" << std::endl;
}

Engine::Engine() :
  //rc( RenderContext::getInstance() ),
  //io( IoMod::getInstance() ),
  //clock( Clock::getInstance() ),
  renderer( RenderContext::getInstance().getRenderer() ),
  //world("back", Gamedata::getInstance().getXmlInt("back/factor") ),
  sky("sky", Gamedata::getInstance().getXmlInt("sky/factor") ),
  fargrass("farGrass", Gamedata::getInstance().getXmlInt("farGrass/factor") ),
  neargrass("nearGrass", Gamedata::getInstance().getXmlInt("nearGrass/factor") ),
  ui( UI::getInstance() ),
  viewport( Viewport::getInstance() ),
  sound(),
  //star(new Sprite("YellowStar")),
  //spinningStar(new MultiSprite("SpinningStar")),
	//sprites({new Sprite("YellowStar"), new MultiSprite("SpinningStar")}),
  player(new Player("sheep")),
	sprites(),
  winTime(0),
  currentSprite(0),
  strategy(new PerPixelCollisionStrategy),
  //collision(false),
  objectiveCollision(nullptr),
  resourceCollision(nullptr),
  makeVideo( false )
{
  Clock::getInstance().startClock();
  //int numRainDrops = Gamedata::getInstance().getXmlInt("RainDrop/noOfSprite");

  sprites.reserve(5 + 3 + 3 + 3 + 1);

  sprites.emplace_back(new Sprite("greenGrass", Vector2f(500, 530), Vector2f(0, 0)));
  sprites.emplace_back(new Sprite("greenGrass", Vector2f(1300, 530), Vector2f(0, 0)));
  sprites.emplace_back(new Sprite("greenGrass", Vector2f(3000, 530), Vector2f(0, 0)));
  sprites.emplace_back(new Sprite("greenGrass", Vector2f(4150, 530),Vector2f(0, 0)));
  sprites.emplace_back(new Sprite("greenGrass", Vector2f(6250, 530), Vector2f(0, 0)));

  sprites.emplace_back(new Sprite("blueGrass", Vector2f(2000, 530), Vector2f(0, 0)));
  sprites.emplace_back(new Sprite("blueGrass", Vector2f(3850, 530), Vector2f(0, 0)));
  sprites.emplace_back(new Sprite("blueGrass", Vector2f(4800, 530), Vector2f(0, 0)));
  //sprites.emplace_back(new Sprite("BlueGrass4"));
  //sprites.emplace_back(new Sprite("BlueGrass5"));

  sprites.emplace_back(new ProximitySprite("flower", player->getPosition(), player->getScaledWidth(),
    player->getScaledHeight(), Vector2f(1150, 530), Vector2f(0, 0)));
  player->attach(static_cast<SmartSprite*> (sprites[8]));

  sprites.emplace_back(new ProximitySprite("flower", player->getPosition(), player->getScaledWidth(),
    player->getScaledHeight(), Vector2f(4000, 530), Vector2f(0, 0)));
  player->attach(static_cast<SmartSprite*> (sprites[9]));

  sprites.emplace_back(new ProximitySprite("flower", player->getPosition(), player->getScaledWidth(),
    player->getScaledHeight(), Vector2f(6020, 530), Vector2f(0, 0)));
  player->attach(static_cast<SmartSprite*> (sprites[10]));

  sprites.emplace_back(new TwoWayMultiSprite("wasp", Vector2f(4000, 100), Vector2f(-400, -400)));
  sprites.emplace_back(new TwoWayMultiSprite("wasp", Vector2f(5000, 100), Vector2f(500, 200)));
  sprites.emplace_back(new TwoWayMultiSprite("wasp", Vector2f(6000, 100), Vector2f(-200, -600)));

  sprites.emplace_back(new TwoWayMultiSprite("ram"));
  //sprites.emplace_back(new Player("Sheep"));

  /*for(int i = 0; i < numRainDrops; i++){
    sprites.emplace_back(new StaggerSprite("RainDrop"));
  }*/
  //Viewport::getInstance().setObjectToTrack(star);
	Viewport::getInstance().setObjectToTrack(player);
  std::cout << "Loading complete" << std::endl;
}

void Engine::draw() const{
  //world.draw();
  sky.draw();
  fargrass.draw();
  neargrass.draw();

  //star->draw();
  //spinningStar->draw();
	for(Drawable *s: sprites){
		s->draw();
	}
  player->draw();

  ui.draw(Clock::getInstance().getSeconds(), Clock::getInstance().getFrametime(),
    player->isGod());
  //viewport.draw(clock.getFrametime());
  if(!player->getHealth()){
    IoMod::getInstance().writeText("You died!", {255, 0, 0, 255}, 600, 350);
  }
  if(!player->getGrassLeft()){
    player->deactivate();

    IoMod::getInstance().writeText("You beat level 1", {255, 0, 0, 255}, 600, 350);

    std::stringstream winmsg;
    winmsg << "in " << winTime << " seconds!";
    IoMod::getInstance().writeText(winmsg.str(), {255, 0, 0, 255}, 600, 380);
  }
  viewport.draw();
  //if(collision) IoMod::getInstance().writeText("OW", {200, 0, 0, 255}, 600, 350);
  SDL_RenderPresent(renderer);
}

void Engine::update(Uint32 ticks) {
  checkForCollisions();
  if(!player->getGrassLeft()){
    if(!winTime) winTime = Clock::getInstance().getSeconds();
  }
  //star->update(ticks);
  //spinningStar->update(ticks);
	for(Drawable *s: sprites){
		s->update(ticks);
	}
  player->update(ticks);
  //world.update();
  sky.update();
  fargrass.update();
  neargrass.update();
  viewport.update(); // always update viewport last
}

void Engine::checkForCollisions(){
  objectiveCollision = nullptr;
  resourceCollision = nullptr;
  //collision = false;
  for(std::vector<Drawable*>::iterator it = sprites.begin(); it != sprites.end(); it++) {
    if(!(*it)->isActive()) continue;
    if((*it)->getType() == Drawable::ENEMY){
      if (strategy->execute(*player, **it)) {
        //collision = true;
        player->takeDamage(Clock::getInstance().getTicks(), sound);
        //break;
        //std::cout << "OW" << std::endl;
      }
      if(player->checkForProjectileCollisions(**it)){
        //std::cout << "hit an enemy" << std::endl;
        (*it)->explode();
      }
    }
    if((*it)->getType() == Drawable::OBJECTIVE){
      if (strategy->execute(*player, **it)) {
        objectiveCollision = *it;
      }
    }
    if((*it)->getType() == Drawable::RESOURCE){
      if (strategy->execute(*player, **it)) {
        resourceCollision = *it;
      }
    }
  }
}

void Engine::switchSprite(){
  ++currentSprite;
  //currentSprite = currentSprite % 2;
  currentSprite = currentSprite % sprites.size(); // % num sprites?
  /*if ( currentSprite ) {
    Viewport::getInstance().setObjectToTrack(spinningStar);
  }
  else {
    Viewport::getInstance().setObjectToTrack(star);
  }*/
	Viewport::getInstance().setObjectToTrack(sprites[currentSprite]);
}

bool Engine::play() {
  SDL_Event event;
  const Uint8* keystate;
  bool done = false;
  Uint32 ticks = Clock::getInstance().getElapsedTicks();
  FrameGenerator frameGen;

  while ( !done ) {
    // The next loop polls for events, guarding against key bounce:
    while ( SDL_PollEvent(&event) ) {
      keystate = SDL_GetKeyboardState(NULL);
      if (event.type ==  SDL_QUIT) { done = true; break; }
      if(event.type == SDL_KEYDOWN) {
        /*if (keystate[SDL_SCANCODE_ESCAPE] || keystate[SDL_SCANCODE_Q]) {
          done = true;
          break;
        }*/
        if (keystate[SDL_SCANCODE_ESCAPE] || keystate[SDL_SCANCODE_M]) {
          done = true;
          break;
        }
        if (keystate[SDL_SCANCODE_F1]) {
          ui.toggle();
        }
        if ( keystate[SDL_SCANCODE_P] ) {
          if ( Clock::getInstance().isPaused() ) Clock::getInstance().unpause();
          else Clock::getInstance().pause();
        }
        if(keystate[SDL_SCANCODE_R]){
          return true;
        }
        if(keystate[SDL_SCANCODE_G]){
          if(player->isGod()) player->becomeMortal();
          else player->becomeGod();
        }
        /*if ( keystate[SDL_SCANCODE_T] ) {
          switchSprite();
        }*/
        /*if (keystate[SDL_SCANCODE_F4] && !makeVideo) {
          std::cout << "Initiating frame capture" << std::endl;
          makeVideo = true;
        }
        else if (keystate[SDL_SCANCODE_F4] && makeVideo) {
          std::cout << "Terminating frame capture" << std::endl;
          makeVideo = false;
        }*/
      }
      if(event.type == SDL_MOUSEBUTTONDOWN){
        if(SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)){
          //std::cout << "left click" << std::endl;
          if(objectiveCollision){
            player->eat(objectiveCollision, sound);
          }
          if(resourceCollision){
            player->eat(resourceCollision, sound);
          }
        }
        if(SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT)){
          //std::cout << "right click" << std::endl;
          player->bah(Clock::getInstance().getTicks(), sound);
        }
      }
    }

    // In this section of the event loop we allow key bounce:

    ticks = Clock::getInstance().getElapsedTicks();
    if ( ticks > 0 ) {
      keystate = SDL_GetKeyboardState(NULL); // why do I need this???
      Clock::getInstance().incrFrame();
      if (keystate[SDL_SCANCODE_A]) {
        player->left();
      }
      if (keystate[SDL_SCANCODE_D]) {
        player->right();
      }
      /*if (keystate[SDL_SCANCODE_W]) {
        player->up();
      }
      if (keystate[SDL_SCANCODE_S]) {
        player->down();
      }*/
      if (keystate[SDL_SCANCODE_SPACE]) {
        player->jump();
      }
      draw();
      update(ticks);
      if ( makeVideo ) {
        frameGen.makeFrame();
      }
    }
  }

  return false;
}