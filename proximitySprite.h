#ifndef PROXIMITYSPRITE__H
#define PROXIMITYSPRITE__H
#include <string>
#include "smartSprite.h"

class ProximitySprite : public SmartSprite {
public:
  ProximitySprite(const std::string&, const Vector2f& pos, int w, int h);
  ProximitySprite(const std::string&, const Vector2f& player_pos, int w, int h, const Vector2f& pos, const Vector2f& vel);
  ProximitySprite(const ProximitySprite&);
  //virtual ~ProximitySprite() { } 

  virtual void update(Uint32 ticks);

private:
  enum MODE {NICE, EVIL};
  MODE currentMode;
  float triggerDistance;

  void updateMode(MODE);
};
#endif