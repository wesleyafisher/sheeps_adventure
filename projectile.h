#ifndef PROJECTILE__H
#define PROJECTILE__H

#include <iostream>
#include "twoWayMultiSprite.h"
#include "gameData.h"

class Projectile : public TwoWayMultiSprite {
public:
  explicit Projectile(const string& name) :
    TwoWayMultiSprite(name), 
    distance(0), 
    maxDistance(Gamedata::getInstance().getXmlInt(name+"/deleteDistance")), 
    tooFar(false)
  { }

  Projectile(const Projectile& )=default;
  Projectile(      Projectile&&)=default;
  Projectile& operator=(const Projectile& )=default;
  Projectile& operator=(      Projectile&&)=default;

  virtual void update(Uint32 ticks);
  bool goneTooFar() const { return tooFar; }
  void reset() {
    tooFar = false;
    distance = 0;
  }

private:
  float distance;
  float maxDistance;
  bool tooFar;
};
#endif
