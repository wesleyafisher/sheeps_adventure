#ifndef TWOWAYMULTISPRITE__H
#define TWOWAYMULTISPRITE__H
#include <string>
#include <vector>
#include <cmath>
#include "drawable.h"

class ExplodingSprite;

class TwoWayMultiSprite : public Drawable {
public:
  virtual ~TwoWayMultiSprite();
  TwoWayMultiSprite(const std::string&);
  TwoWayMultiSprite(const std::string&, const Vector2f& pos, const Vector2f& vel);
  TwoWayMultiSprite(const TwoWayMultiSprite&);

  virtual void draw() const;
  virtual void uiDraw() const;
  virtual void update(Uint32 ticks);
  virtual void explode();

  virtual const Image* getImage() const { 
    return images[currentFrame]; 
  }
  int getScaledWidth()  const { 
    return getScale()*images[currentFrame]->getWidth(); 
  } 
  int getScaledHeight()  const { 
    return getScale()*images[currentFrame]->getHeight(); 
  } 
  virtual const SDL_Surface* getSurface() const { 
    return images[currentFrame]->getSurface();
  }

protected:
  std::vector<Image *> images;
  ExplodingSprite* explosion;

  unsigned currentFrame;
  unsigned numberOfFrames;
  unsigned frameInterval;
  float timeSinceLastFrame;
  int worldWidth;
  int worldHeight;

  void advanceFrame(Uint32 ticks, float xVelocity);
  unsigned rightWrap(unsigned num) { return num % (numberOfFrames / 2); }
  unsigned leftWrap(unsigned num) { return (num % (numberOfFrames / 2)) + (numberOfFrames / 2); }
  TwoWayMultiSprite& operator=(const TwoWayMultiSprite&);
};
#endif
