#include "smartSprite.h"
#include "gameData.h"
#include "renderContext.h"
#include "explodingSprite.h"

SmartSprite::~SmartSprite(){if(explosion) delete explosion;}

SmartSprite::SmartSprite( const std::string& name, const Vector2f& pos, int w, int h) :
  Drawable(name, 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/startLoc/x"), 
                    Gamedata::getInstance().getXmlInt(name+"/startLoc/y")), 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/speedX"),
                    Gamedata::getInstance().getXmlInt(name+"/speedY")),
           Gamedata::getInstance().getXmlInt(name+"/type")
           ),
  images( RenderContext::getInstance().getImages(name) ),
  explosion(nullptr),
  playerPos(pos),
  playerWidth(w),
  playerHeight(h),
  currentFrame(0),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height"))
{ }

SmartSprite::SmartSprite( const std::string& name, const Vector2f& player_pos, int w, int h,
  const Vector2f& pos, const Vector2f& vel) :
  Drawable(name, pos, vel, Gamedata::getInstance().getXmlInt(name+"/type")),
  images( RenderContext::getInstance().getImages(name) ),
  explosion(nullptr),
  playerPos(player_pos),
  playerWidth(w),
  playerHeight(h),
  currentFrame(0),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height"))
{ }

SmartSprite::SmartSprite(const SmartSprite& s) :
  Drawable(s), 
  images(s.images),
  explosion(s.explosion),
  playerPos(s.playerPos),
  playerWidth(s.playerWidth),
  playerHeight(s.playerHeight),
  currentFrame(s.currentFrame),
  numberOfFrames( s.numberOfFrames ),
  worldWidth( s.worldWidth ),
  worldHeight( s.worldHeight )
  { }

SmartSprite& SmartSprite::operator=(const SmartSprite& s) {
  Drawable::operator=(s);
  images = (s.images);
  explosion = (s.explosion);
  playerPos = (s.playerPos);
  playerWidth = (s.playerWidth);
  playerHeight = (s.playerHeight);
  currentFrame = (s.currentFrame);
  numberOfFrames = ( s.numberOfFrames );
  worldWidth = ( s.worldWidth );
  worldHeight = ( s.worldHeight );
  return *this;
}

void SmartSprite::explode() {
  if ( !explosion ) {
    Sprite 
    sprite(getName(), getPosition(), getVelocity(), images[currentFrame]);
    explosion = new ExplodingSprite(sprite);
  }
  deactivate();
}

void SmartSprite::draw() const {
  if(explosion) explosion->draw();
  if(isActive()) images[currentFrame]->draw(getX(), getY(), getScale());
}

void SmartSprite::uiDraw() const {
  if(explosion) explosion->draw();
  if(isActive()) images[currentFrame]->uiDraw(getX(), getY(), getScale());
}

void SmartSprite::update(Uint32 ticks) { 
  if ( explosion ) {
    explosion->update(ticks);
    /*if ( explosion->chunkCount() == 0 ) {
      delete explosion;
      explosion = NULL;
      activate();
    }*/
    return;
  }
  if(!isActive()) return;

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr);

  if ( getY() < 0) {
    setVelocityY( fabs( getVelocityY() ) );
  }
  if ( getY() > worldHeight-getScaledHeight()) {
    setVelocityY( -fabs( getVelocityY() ) );
  }

  if ( getX() < 0) {
    setVelocityX( fabs( getVelocityX() ) );
  }
  if ( getX() > worldWidth-getScaledWidth()) {
    setVelocityX( -fabs( getVelocityX() ) );
  }  

}
