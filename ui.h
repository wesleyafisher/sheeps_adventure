#ifndef UI__H
#define UI__H
#include <SDL.h>
#include <vector>
#include "drawable.h"

class UI {
public:
  static UI& getInstance();
  ~UI ();
  //void draw(int) const;
  void draw(unsigned, unsigned, bool) const;
  void toggle();

private:
  bool on;
  SDL_Renderer* renderer;
  SDL_Rect uiWindow;

  UI();
  UI(const UI&);
  UI& operator=(const UI&);
};
#endif
