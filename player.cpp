#include "player.h"
#include "gameData.h"
#include "renderContext.h"
#include "explodingSprite.h"
#include "ioMod.h"

void Player::advanceFrame(Uint32 ticks, const float xVelocity){
  timeSinceLastFrame += ticks;

	if (timeSinceLastFrame > frameInterval) {
    // Last moving velocity of the sprite
    //static float lastXvelocity = 0.0;

    // The sprite isn't moving
    if(xVelocity == 0.0){
      // It was going right the last time it was moving
      if(lastXvelocity >= 0.0) currentFrame = rightWrap(0);

      // It going left the last time it was moving
      else currentFrame = leftWrap(0);
    }

    // The sprite is moving to the right
    else if(xVelocity > 0.0){
      currentFrame = rightWrap(currentFrame + 1);
      lastXvelocity = xVelocity;
    }

    // The sprite is moving to the left
    else{
      currentFrame = leftWrap(currentFrame + 1);
      lastXvelocity = xVelocity;
    }

    timeSinceLastFrame = 0;
	}

}

Player::~Player() {
  for ( Projectile *p : ammoPool ) {
    delete p;
  }
  for ( Projectile *p : freePool ) {
    delete p;
  }
  ammoPool.clear();
  freePool.clear();

  for(Drawable *s : filledHP){
    delete s;
  }
  for(Drawable *s : emptyHP){
    delete s;
  }
  for(Drawable *s : filledAmmo){
    delete s;
  }
  for(Drawable *s : emptyAmmo){
    delete s;
  }

  delete strategy;
  if(explosion) delete explosion;
  delete grassSprite;
}

Player::Player( const std::string& name) :
  Drawable(name, 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/startLoc/x"), 
                    Gamedata::getInstance().getXmlInt(name+"/startLoc/y")), 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/speedX"),
                    Gamedata::getInstance().getXmlInt(name+"/speedY")),
           Gamedata::getInstance().getXmlInt(name+"/type")
           ),
  images( RenderContext::getInstance().getImages(name) ),
  observers(),
  ammoPool(),
  freePool(),
  filledHP(),
  emptyHP(),
  filledAmmo(),
  emptyAmmo(),
  strategy(new PerPixelCollisionStrategy),
  explosion(nullptr),
  grassSprite(new Sprite("greenGrass", Vector2f(1150, 100), Vector2f(0, 0))),
  grassLeftRect({1235, 125, 25, 30}),
  currentFrame(0),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  frameInterval( Gamedata::getInstance().getXmlInt(name+"/frameInterval")),
  timeSinceLastFrame( 0 ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),
  initialVelocity(getVelocity()),
  lastXvelocity(0.0),
  gravity(Gamedata::getInstance().getXmlInt("world/gravity")),
  maxHealth(Gamedata::getInstance().getXmlInt("player/maxHealth")),
  currentHealth(maxHealth),
  iframeDuration(Gamedata::getInstance().getXmlInt("player/iframeDuration")),
  lastTickHit(0),
  bahBlastCooldown(Gamedata::getInstance().getXmlInt("player/bahBlastCooldown")),
  lastTickBahBlast(0),
  maxAmmo(Gamedata::getInstance().getXmlInt("player/maxAmmo")),
  currentAmmo(0),
  grassLeft(5),
  god(false)
{
  SDL_SetRenderDrawBlendMode(RenderContext::getInstance().getRenderer(), SDL_BLENDMODE_BLEND);
  SDL_SetRenderDrawColor(RenderContext::getInstance().getRenderer(), 0, 0, 0, 140);

  grassSprite->setScale(0.5);

  filledHP.reserve(maxHealth); emptyHP.reserve(maxHealth);
  filledAmmo.reserve(maxAmmo); emptyAmmo.reserve(maxAmmo);

  for(int i = 0; i < maxHealth; i++){
    filledHP.emplace_back(new Sprite("fullHeart", Vector2f(1230 - (50 * i), 0), Vector2f(0, 0)));
    emptyHP.emplace_back(new Sprite("emptyHeart", Vector2f(1230 - (50 * i), 0), Vector2f(0, 0)));
  }
  for(int i = 0; i < maxAmmo; i++){
    filledAmmo.emplace_back(new Sprite("fullAmmo", Vector2f(1230 - (50 * i), 50), Vector2f(0, 0)));
    emptyAmmo.emplace_back(new Sprite("emptyAmmo", Vector2f(1230 - (50 * i), 50), Vector2f(0, 0)));
  } 
}

Player::Player(const Player& s) :
  Drawable(s), 
  images(s.images),
  observers(s.observers),
  ammoPool( s.ammoPool ),
  freePool( s.freePool ),
  filledHP( s.filledHP ),
  emptyHP( s.emptyHP ),
  filledAmmo( s.filledAmmo ),
  emptyAmmo( s.emptyAmmo ),
  strategy( s.strategy ),
  explosion(s.explosion),
  grassSprite(s.grassSprite),
  grassLeftRect(s.grassLeftRect),
  currentFrame(s.currentFrame),
  numberOfFrames( s.numberOfFrames ),
  frameInterval( s.frameInterval ),
  timeSinceLastFrame( s.timeSinceLastFrame ),
  worldWidth( s.worldWidth ),
  worldHeight( s.worldHeight ),
  initialVelocity( s.initialVelocity ),
  lastXvelocity(s.lastXvelocity),
  gravity( s.gravity ),
  maxHealth( s.maxHealth ),
  currentHealth( s.currentHealth ),
  iframeDuration( s.iframeDuration ),
  lastTickHit( s.lastTickHit ),
  bahBlastCooldown ( s.bahBlastCooldown ),
  lastTickBahBlast ( s.lastTickBahBlast ),
  maxAmmo( s.maxAmmo ),
  currentAmmo( s.currentAmmo ),
  grassLeft( s.grassLeft ),
  god(s.god)
  { }

Player& Player::operator=(const Player& s) {
  Drawable::operator=(s);
  images = (s.images);
  observers = (s.observers);
  ammoPool = (s.ammoPool);
  freePool = (s.freePool);
  filledHP = ( s.filledHP );
  emptyHP = ( s.emptyHP );
  filledAmmo = ( s.filledAmmo );
  emptyAmmo = ( s.emptyAmmo );
  strategy = (s.strategy);
  explosion = (s.explosion);
  grassSprite = (s.grassSprite);
  grassLeftRect = (s.grassLeftRect);
  currentFrame = (s.currentFrame);
  numberOfFrames = ( s.numberOfFrames );
  frameInterval = ( s.frameInterval );
  timeSinceLastFrame = ( s.timeSinceLastFrame );
  worldWidth = ( s.worldWidth );
  worldHeight = ( s.worldHeight );
  initialVelocity = ( s.initialVelocity );
  lastXvelocity = (s.lastXvelocity);
  gravity = ( s.gravity );
  maxHealth = ( s.maxHealth );
  currentHealth = ( s.currentHealth );
  iframeDuration = ( s.iframeDuration );
  lastTickHit = ( s.lastTickHit );
  bahBlastCooldown = (s.bahBlastCooldown);
  lastTickBahBlast = (s.lastTickBahBlast);
  maxAmmo = ( s.maxAmmo );
  currentAmmo = ( s.currentAmmo );
  grassLeft = (s.grassLeft);
  god = (s.god);
  return *this;
}

void Player::explode() {
  if ( !explosion ) {
    Sprite 
    sprite(getName(), getPosition(), getVelocity(), images[currentFrame]);
    explosion = new ExplodingSprite(sprite);
  }
  deactivate();
}

void Player::draw() const {
  if(explosion) explosion->draw();
  if(isActive()) images[currentFrame]->draw(getX(), getY(), getScale());
  for(Projectile *p: ammoPool){
    p->draw();
  }
  for(int i = 0; i < maxHealth; i++){
    if(currentHealth > i) filledHP[i]->uiDraw();
    else emptyHP[i]->uiDraw();
  }
  for(int i = 0; i < maxAmmo; i++){
    if(currentAmmo > i) filledAmmo[i]->uiDraw();
    else emptyAmmo[i]->uiDraw();
  }
  grassSprite->uiDraw();
  SDL_RenderFillRect(RenderContext::getInstance().getRenderer(), &grassLeftRect);
  IoMod::getInstance().writeText(std::to_string(grassLeft), {255, 255, 255, 255}, 1240, 125);
}

void Player::uiDraw() const {
  if(explosion) explosion->uiDraw();
  if(isActive()) images[currentFrame]->uiDraw(getX(), getY(), getScale());
  for(Projectile *p: ammoPool){
    p->uiDraw();
  }
  for(int i = 0; i < maxHealth; i++){
    if(currentHealth > i) filledHP[i]->uiDraw();
    else emptyHP[i]->uiDraw();
  }
  for(int i = 0; i < maxAmmo; i++){
    if(currentAmmo > i) filledAmmo[i]->uiDraw();
    else emptyAmmo[i]->uiDraw();
  }
  grassSprite->uiDraw();
  SDL_RenderFillRect(RenderContext::getInstance().getRenderer(), &grassLeftRect);
  IoMod::getInstance().writeText(std::to_string(grassLeft), {255, 255, 255, 255}, 1240, 125);
}

void Player::detach(SmartSprite *o){
  for(std::vector<SmartSprite*>::iterator ptr = observers.begin();
    ptr != observers.end(); ptr++) {
    if (*ptr == o) {
      ptr = observers.erase(ptr);
      return;
    }
  }
}

bool Player::checkForProjectileCollisions(Drawable &other) const {
  bool collision = false;
  for(std::list<Projectile*>::const_iterator it = ammoPool.begin(); it != ammoPool.end(); it++){
    if(!(*it)->isActive()) continue;
    if(strategy->execute(other, **it)){
      collision = true;
      break;
    }
  }
  return collision;
}

void Player::right() {
  if(!isActive()) return;
  if ( getX() < worldWidth-getScaledWidth()) {
    setVelocityX(initialVelocity[0]);
  }
}

void Player::left()  {
  if(!isActive()) return;
  if ( getX() > 0) {
    setVelocityX(-initialVelocity[0]);
  }
}

void Player::jump() { 
  if(!isActive()) return;
  if ( getY() >= worldHeight-getScaledHeight()-40 ) {
    setVelocityY( -initialVelocity[1] );
  }
}

void Player::stop() {
  if(!isActive()) return;
  setVelocityX(0);
  if ( getY() >= worldHeight-getScaledHeight()-40 ) {
    setVelocityY(0);
  }
  else setVelocityY(getVelocityY() + gravity);
}

void Player::takeDamage(unsigned currentTick, SDLSound& sound){
  if(!isActive()) return;
  if(god) return;
  if((currentTick - lastTickHit) < iframeDuration) return;
  lastTickHit = currentTick;

  //std::cout << "Got hit, health is " << currentHealth << std::endl;
  if(currentHealth <= 0) return;
  sound[3];
  currentHealth--;
  if(currentHealth <= 0){
    //currentHealth = maxHealth;
    sound[1];
    explode();
  }
}

void Player::eat(Drawable* eatTarget, SDLSound& sound){
  if(!isActive()) return;
  sound[2];
  if(eatTarget->getType() == Drawable::OBJECTIVE){
    //std::cout << "eating greengrass" << std::endl;
    if(grassLeft > 0) grassLeft--;
    eatTarget->explode();
  }
  if(eatTarget->getType() == Drawable::RESOURCE){
    //std::cout << "eating bluegrass" << std::endl;
    if(currentAmmo >= maxAmmo) return;
    currentAmmo++;
    eatTarget->explode();
  }

}

void Player::bah(unsigned currentTick, SDLSound& sound){
  if(!isActive()) return;
  if(currentAmmo <= 0) return;

  if((currentTick - lastTickBahBlast) < bahBlastCooldown) return;
  lastTickBahBlast = currentTick;

  sound[0];

  // Shoot bahBlast
  Projectile* bahBlast = nullptr;

  // Check if there is a bahBlast in the free pool
  if(!freePool.empty()){
    bahBlast = freePool.back();
    freePool.pop_back();
  }
  else{
    bahBlast = new Projectile("bahBlast");
  }

  // Set speed and position of bahBlast
  bool right = (currentFrame < numberOfFrames / 2 ? true : false);
  bahBlast->reset();

  float yveloc = Gamedata::getInstance().getXmlInt("bahBlast/speedX");
  if (!right) yveloc *= -1.0;

  float xpos = getX();
  if(right) xpos += 200.0;
  else xpos -= 200.0;

  bahBlast->setVelocity(Vector2f(yveloc, 0));
  bahBlast->setPosition(Vector2f(xpos, getY()));

  // shoot bahBlast
  ammoPool.push_back(bahBlast);

  currentAmmo--;
}

void Player::update(Uint32 ticks) {
  std::list<Projectile*>::iterator ptr = ammoPool.begin();
  while (ptr != ammoPool.end()) {
    (*ptr)->update(ticks);
    if ( (*ptr)->goneTooFar()) {  // Check to see if we should free a chunk
      freePool.push_back(*ptr);
      ptr = ammoPool.erase(ptr);
    }   
    else ++ptr;
  }

  if ( explosion ) {
    explosion->update(ticks);
    /*if ( explosion->chunkCount() == 0 ) {
      delete explosion;
      explosion = NULL;
      activate();
    }*/
    return;
  }
  if(!isActive()) return;

  advanceFrame(ticks, getVelocity()[0]);

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr);

  /*if ( getY() < 0) {
    setVelocityY( fabs( getVelocityY() ) );
  }
  if ( getY() > worldHeight-getScaledHeight()) {
    setVelocityY( -fabs( getVelocityY() ) );
  }

  if ( getX() < 0) {
    setVelocityX( fabs( getVelocityX() ) );
  }
  if ( getX() > worldWidth-getScaledWidth()) {
    setVelocityX( -fabs( getVelocityX() ) );
  }*/

  for(std::vector<SmartSprite*>::iterator ptr = observers.begin();
    ptr != observers.end(); ptr++) {
    (*ptr)->setPlayerPos( getPosition() );
  }

  stop();

}
