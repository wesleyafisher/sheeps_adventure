#ifndef SMARTSPRITE__H
#define SMARTSPRITE__H
#include <string>
#include <vector>
#include <cmath>
#include "drawable.h"

class ExplodingSprite;

class SmartSprite : public Drawable {
public:
  ~SmartSprite();
  SmartSprite(const std::string&, const Vector2f& pos, int w, int h);
  SmartSprite(const std::string&, const Vector2f& player_pos, int w, int h, const Vector2f& pos, const Vector2f& vel);
  SmartSprite(const SmartSprite&);

  virtual void draw() const;
  virtual void uiDraw() const;
  virtual void update(Uint32 ticks);
  virtual void explode();

  void setPlayerPos(const Vector2f& p) { playerPos = p; }

  virtual const Image* getImage() const { 
    return images[currentFrame]; 
  }
  int getScaledWidth()  const { 
    return getScale()*images[currentFrame]->getWidth(); 
  } 
  int getScaledHeight()  const { 
    return getScale()*images[currentFrame]->getHeight(); 
  } 
  virtual const SDL_Surface* getSurface() const { 
    return images[currentFrame]->getSurface();
  }

protected:
  std::vector<Image*> images;
  ExplodingSprite* explosion;

  Vector2f playerPos;
  int playerWidth;
  int playerHeight;
  unsigned currentFrame;
  unsigned numberOfFrames;
  int worldWidth;
  int worldHeight;

  void advanceFrame(Uint32 ticks);
  SmartSprite& operator=(const SmartSprite&);
};
#endif
