#include <sstream>
#include "viewPort.h"
#include "ioMod.h"

Viewport& Viewport::getInstance() {
  static Viewport viewport;
  return viewport;
}

Viewport::Viewport() : 
  //gdata(Gamedata::getInstance()),
  position(0, 0),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),
  viewWidth(Gamedata::getInstance().getXmlInt("view/width")), 
  viewHeight(Gamedata::getInstance().getXmlInt("view/height")),
  objWidth(0), objHeight(0),
  objectToTrack(NULL) 
{}

void Viewport::setObjectToTrack(const Drawable *obj) { 
  objectToTrack = obj; 
  objWidth = objectToTrack->getScaledWidth();
  objHeight = objectToTrack->getScaledHeight();
}

//void Viewport::draw(int frametime) const {
void Viewport::draw() const {

  // Write "Frametime:#"
  //std::stringstream frametimestrm;
  //frametimestrm << "Frametime: " << frametime << "ms";
  //std::string currentFrametime = frametimestrm.str();
  //IoMod::getInstance().writeText(currentFrametime, {0, 0, 0, 255}, 10, 10);

  // Write name
  //std::stringstream namestrm;
  //namestrm << "Wesley Fisher";
  //std::string name = namestrm.str();
  //IoMod::getInstance().writeText(namestrm.str(), {0, 0, 0, 255}, 10, 570);

  //IoMod::getInstance().writeText("Tracking: "+objectToTrack->getName(), 30, 30);
}

void Viewport::update() {
  const float x = objectToTrack->getX();
  const float y = objectToTrack->getY();

  position[0] = (x + objWidth/2) - viewWidth/2;
  position[1] = (y + objHeight/2) - viewHeight/2;
  if (position[0] < 0) position[0] = 0;
  if (position[1] < 0) position[1] = 0;
  if (position[0] > (worldWidth - viewWidth)) {
    position[0] = worldWidth-viewWidth;
  }
  if (position[1] > (worldHeight - viewHeight)) {
    position[1] = worldHeight-viewHeight;
  }
}
