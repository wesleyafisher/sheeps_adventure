#include <cmath>
#include <random>
#include <functional>
#include "sprite.h"
#include "gameData.h"
#include "renderContext.h"
#include "explodingSprite.h"

Sprite::~Sprite() { if(explosion) delete explosion; }

Sprite::Sprite(const std::string& name) :
  Drawable(name,
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/startLoc/x"), 
                    Gamedata::getInstance().getXmlInt(name+"/startLoc/y")), 
           Vector2f(
                    Gamedata::getInstance().getXmlInt(name+"/speedX"), // normal speed
                    Gamedata::getInstance().getXmlInt(name+"/speedY")),
           Gamedata::getInstance().getXmlInt(name+"/type") 
           ),
  image( RenderContext::getInstance().getImage(name) ),
  explosion(nullptr),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height"))
{ }

Sprite::Sprite(const string& n, const Vector2f& pos, const Vector2f& vel) :
  Drawable(n, pos, vel, Gamedata::getInstance().getXmlInt(n+"/type")), 
  image( RenderContext::getInstance().getImage(n) ),
  explosion(nullptr),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height"))
{ }

Sprite::Sprite(const string& n, const Vector2f& pos, const Vector2f& vel,
               const Image* img):
  Drawable(n, pos, vel, Gamedata::getInstance().getXmlInt(n+"/type")), 
  image( img ),
  explosion(nullptr),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height"))
{ }

Sprite::Sprite(const Sprite& s) :
  Drawable(s), 
  image(s.image),
  explosion(s.explosion),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height"))
{ }

Sprite& Sprite::operator=(const Sprite& rhs) {
  Drawable::operator=( rhs );
  image = rhs.image;
  explosion = rhs.explosion;
  worldWidth = rhs.worldWidth;
  worldHeight = rhs.worldHeight;
  return *this;
}

inline namespace{
  constexpr float SCALE_EPSILON = 2e-7;
}

void Sprite::explode() {
  if ( !explosion ) explosion = new ExplodingSprite(*this);
  deactivate();
}

void Sprite::draw() const { 
  if(getScale() < SCALE_EPSILON) return;
  if(explosion) explosion->draw();
  if(isActive()) image->draw(getX(), getY(), getScale()); 
}

void Sprite::uiDraw() const { 
  if(getScale() < SCALE_EPSILON) return;
  if(explosion) explosion->uiDraw();
  if(isActive()) image->uiDraw(getX(), getY(), getScale()); 
}

void Sprite::update(Uint32 ticks) {
  if ( explosion ) {
    explosion->update(ticks);
    /*if ( explosion->chunkCount() == 0 ) {
      delete explosion;
      explosion = NULL;
      activate();
    }*/
    return;
  }
  if(!isActive()) return;

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr);

  if ( getY() < 0) {
    setVelocityY( std::abs( getVelocityY() ) );
  }
  if ( getY() > worldHeight-getScaledHeight()) {
    setVelocityY( -std::abs( getVelocityY() ) );
  }

  if ( getX() < 0) {
    setVelocityX( std::abs( getVelocityX() ) );
  }
  if ( getX() > worldWidth-getScaledWidth()) {
    setVelocityX( -std::abs( getVelocityX() ) );
  }  
}