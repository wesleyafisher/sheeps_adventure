#include <cmath>
#include <random>
#include <functional>
#include "proximitySprite.h"
#include "gameData.h"
#include "renderContext.h"

float distance(float x1, float y1, float x2, float y2) {
  float x = x1-x2;
  float y = y1-y2;
  return hypot(x, y);
}

ProximitySprite::ProximitySprite(const std::string& name, const Vector2f& pos, 
  int w, int h) :
  SmartSprite(name, pos, w, h),
  currentMode(NICE),
  triggerDistance(Gamedata::getInstance().getXmlFloat(name+"/triggerDistance"))
{}

ProximitySprite::ProximitySprite(const std::string& name, const Vector2f& player_pos, 
  int w, int h, const Vector2f& pos, const Vector2f& vel) :
  SmartSprite(name, player_pos, w, h, pos, vel),
  currentMode(NICE),
  triggerDistance(Gamedata::getInstance().getXmlFloat(name+"/triggerDistance"))
{}

ProximitySprite::ProximitySprite(const ProximitySprite& s) : 
  SmartSprite(s),
  currentMode(s.currentMode),
  triggerDistance(s.triggerDistance)
{}

void ProximitySprite::updateMode(MODE newMode){
  currentFrame = newMode;
}

void ProximitySprite::update(Uint32 ticks) { 
  SmartSprite::update(ticks);
  float x= getX()+getImage()->getWidth()/2;
  float y= getY()+getImage()->getHeight()/2;
  float ex= playerPos[0]+playerWidth/2;
  float ey= playerPos[1]+playerHeight/2;
  float distanceToPlayer = ::distance( x, y, ex, ey );

  if  ( currentMode == NICE ) {
    if(distanceToPlayer < triggerDistance){
      currentMode = EVIL;
      updateMode(currentMode);
    }
  }
  else if  ( currentMode == EVIL ) {
    if(distanceToPlayer > triggerDistance){
      currentMode = NICE;
      updateMode(currentMode);
    }
  }
}
