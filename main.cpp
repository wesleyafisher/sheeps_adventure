// Wesley Fisher       Data-Driven Object Oriented Game Construction
//#include "engine.h"
#include "menuEngine.h"

//ImageFactory*  ImageFactory::instance = NULL;
// Meyers singleton doesn't need this!
//RenderContext* RenderContext::instance = NULL;

int main() {
  //bool keepPlaying = true;
  try {
    /*while(keepPlaying){
      Engine* engine = new Engine;
      keepPlaying = engine->play();
      delete engine;
    }*/
    MenuEngine menu;
    menu.play();
    //delete RenderContext::getInstance();
  }
  catch (const string& msg) { std::cout << msg << std::endl; }
  catch (...) {
    std::cout << "Oops, someone threw an exception!" << std::endl;
  }
  return 0;
}
